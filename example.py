import argparse
from discordapi import Bot, Command, StartCommand, StopCommand, UserWhitelistPermission
from sympy.parsing.sympy_parser import parse_expr

async def log(bot, msg):
    print("{}: {}".format(msg.author, msg.content))

class EchoCommand(Command):
    def __init__(self):
        x = argparse.ArgumentParser()
        x.add_argument("text", help="The string to echo")
        x.add_argument("-n", "--num_times", type=int, default=1, help="Number of times to echo text (between 1 and 5)")
        EchoCommand.parse_args = Command.gen_parse_args(x)
        # OR PASS A STRING
        # EchoCommand.parse_args = Command.gen_parse_args("{text}")
    
    def get_name(self):
        return "echo"
    def get_permission(self):
        return UserWhitelistPermission(["USERNAME#xxxx"])
    async def run(self, client, args, msg, state):
        x = min(5, max(1, args["num_times"]))
        t = args["text"]
        for i in range(x):
            await client.send_message(msg.channel, "ECHO: {}".format(t))
class EvalCommand(Command):
    def parse_args(self, args_str, command_format="/{command}"):
        return {"expr": args_str}
    def get_name(self):
        return "eval"
    async def run(self, client, args, msg, state):
        await client.send_message(msg.channel, "`"+str(parse_expr(args["expr"]))+"`")
x = Bot([StartCommand(), StopCommand(), EchoCommand(), EvalCommand()],
    "YOUR_TOKEN_GOES_HERE",
    custom_funcs=[log])
x.run()
