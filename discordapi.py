import argparse
import io
import shlex
from contextlib import redirect_stderr, redirect_stdout
from abc import ABCMeta, abstractmethod
import re
import discord
from discord import Status

class Bot:
    def __init__(self, commands, token, command_format="/{command}", custom_funcs=[]):
        self.client = discord.Client()
        self.token = token
        self.commands = commands
        self.command_format = command_format
        self.custom_funcs = custom_funcs

        self.on_ready = self.client.event(self.on_ready)
        self.on_message = self.client.event(self.on_message)

        self.attempt_command_regex = re.compile(command_format.replace("{command}", ".*"))
        self.state = {}

    async def on_ready(self):
        print("Started.")
        print("Logged in as", self.client.user.name)
        print("with ID", self.client.user.id)
        print("---------------------")
    async def on_message(self, msg):
        for func in self.custom_funcs:
            await func(self, msg)
            if "RETURN" in self.state:
                del self.state["RETURN"]
                return
        args_str = None
        matches = []
        for c in self.commands:
            x = c.get_args_str(msg, self.command_format)
            if x is not None:
                args_str = x
                matches.append(c)
        if "STOPPED" in self.state and (len(matches) == 0 or len(matches) > 1 or not type(matches[0]).allow_on_stopped):
            return
        if len(matches) == 0:
            # no commands matched
            if self.attempt_command_regex.match(msg.content):
                await self.client.send_message(msg.channel, "Invalid command")
            return
        if len(matches) > 1:
            await self.client.send_message(msg.channel, "Your message matched multiple commands, "
                        + str(list(map(lambda x: x.get_name(), matches))))
            return
        if matches[0].has_permission(msg.author):
            args = matches[0].parse_args(args_str, self.command_format)
            if isinstance(args, str):
                await self.client.send_message(msg.channel, "Error:\n {}".format(args))
                return
            await matches[0].run(self.client, args, msg, self.state)
        else:
            await self.client.send_message(msg.channel, "You don't have permission to run "+matches[0].get_name())

    def run(self):
        self.client.run(self.token)


class Command:
    __metaclass__ = ABCMeta
    allow_on_stopped = False

    @abstractmethod
    def get_name(self):
        pass
    @abstractmethod
    def parse_args(self):
        pass
    @abstractmethod
    async def run(self, client, args, msg, state):
        pass
    def get_permission(self):
        # no restrictions by default
        return UserBlacklistPermission([])

    def has_permission(self, user):
        return self.get_permission().has_permission(user)

    def gen_parse_args(arg_parser):
        def parse_args(self, args_str, command_format="/{command}"):
            # returns dictionary of tokens if parsing successful
            # a string with the error message otherwise
            if isinstance(arg_parser, str):
                tokens = arg_parser.split()
                p_tokens = args_str.split()
                if len(tokens) != len(p_tokens):
                    return "Expected {} token(s), got {} token(s)".format(len(tokens), len(p_tokens))
                my_args = {}
                for i in range(len(tokens)):
                    token = tokens[i]
                    p_token = p_tokens[i]
                    if token[0] == "{" and token[-1] == "}":
                        my_args[token[1:len(token)-1]] = p_token
                    elif token != p_token:
                        return "Expected {} on argument {}, got {}".format(token, i+1, p_token)
                return my_args
            elif isinstance(arg_parser, argparse.ArgumentParser):
                try:
                    s = io.StringIO()
                    with redirect_stderr(s):
                        with redirect_stdout(s):
                            a = shlex.split(args_str)
                            b = arg_parser.parse_args(a)
                            return vars(b)
                except:
                    command_str = command_format.format(command=self.get_name())
                    return re.sub("\S*.py", command_str, s.getvalue())
            else:
                Exception("Unrecognized arg_parser supplied to args decorator: {}".format(arg_parser))
        return parse_args

    def get_args_str(self, msg, command_format):
        # Return args string if this command was attempted
        # None otherwise
        x = command_format.replace("{command}", self.get_name())
        if msg.content.startswith(x):
            return msg.content[len(x):]
        return None

class StartCommand(Command):
    allow_on_stopped = True
    def __init__(self):
        StartCommand.parse_args = Command.gen_parse_args("") # no args
    def get_name(self):
        return "start"
    async def run(self, client, args, msg, state):
        if "STOPPED" not in state:
            await client.send_message(msg.channel, "Server already started.")
            return
        del state["STOPPED"]
        await client.change_presence(status=Status.online)
        await client.send_message(msg.channel, "Started.")
 
class StopCommand(Command):
    allow_on_stopped = True
    def __init__(self):
        StopCommand.parse_args = Command.gen_parse_args("") # no args
    def get_name(self):
        return "stop"
    async def run(self, client, args, msg, state):
        if "STOPPED" in state:
            await client.send_message(msg.channel, "Server already stopped.")
            return
        state["STOPPED"] = 1
        await client.change_presence(status=Status.invisible)
        await client.send_message(msg.channel, "Stopped.")
   
class Permission:
    __metaclass__ = ABCMeta

    @abstractmethod
    def has_permission(self, user):
        # returns a boolean
        pass

class UserWhitelistPermission(Permission):
    def __init__(self, usernames):
        self.usernames = usernames
    def has_permission(self, user):
        if str(user) in self.usernames:
            return True
        return False
class UserBlacklistPermission(Permission):
    def __init__(self, usernames):
        self.usernames = usernames
    def has_permission(self, user):
        if str(user) not in self.usernames:
            return True
        return False
class RoleWhitelistPermission(Permission):
    def __init__(self, role_names):
        self.roles = role_names
    def has_permission(self, user):
        if any([x in self.roles for x in map(lambda x: x.name, user.roles)]):
            return True
        return False
class RoleBlacklistPermission(Permission):
    def __init__(self, role_names):
        self.roles = role_names
    def has_permission(self, user):
        if not any([x in self.roles for x in map(lambda x: x.name, user.roles)]):
            return True
        return False


